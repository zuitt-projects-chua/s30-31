const mongoose = require('mongoose');

/*
Mongoose schema
Before we can create documents from our api to save in our database, we first have to determine the structure of our documents to be written in our database
It is like a blue print for our document
Schema is a constructor from mongoose that will alow us to create new schema
*/

const taskSchema = new mongoose.Schema(
/*
Define the fields ffor our task document
The task document will have name and status field
both of the field should have a databse type of string
fields are defined by the developer
*/
		{
			name: String,
			status: String
		}
	)

/*
Models are use to connect our api to the corresponding collection in your database. it is a representation of the task document

Models use eschema to create objects taht corresponding to the schema. By the default, when creating the collection from your model, the collection name is pluralized

"task" is defined by the developer

mongoose.model(<nameofcolectioninAtlas>, <schematofollow>)
*/

module.exports = mongoose.model("Task", taskSchema)

/*
mongoose.model(<collection>, schema)
module exports will alows us to export files functions and be able to import require them in another file within our application

Export the model into other files that may require it
*/