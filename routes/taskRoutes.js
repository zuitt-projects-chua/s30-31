
const express = require('express');
//require to import express for the use of  router
const router = express.Router();
//allows us to access our http method routes 
const taskControllers = require('../controllers/taskControllers')
//importing taskControllers

//create task
router.post('/', taskControllers.createTaskController);
//retrieving task
router.get('/', taskControllers.getAllTasksControllers);
//retrieving single task
router.get('/getSingleTask/:id', taskControllers.getSingleTaskController)

//updating single task
router.put('/updateTaskStatus/:id', taskControllers.updateTaskStatusController)

module.exports = router;



