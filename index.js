const express = require('express');

const mongoose = require('mongoose');
/*
All packages/modules should be required at the top of the file to avoid tampering or errors 



Mogoose is a package that uses an odm object document mapper allows us to translate our js objects into database documents for mongodb
allows us connection and easier manipulation of our documents in mongodb
*/

const taskRoutes = require('./routes/taskRoutes')
const userRoutes = require('./routes/userRoutes')

const app = express();

mongoose.connect("mongodb+srv://admin_chua:batch169@batch169chua.g8jtg.mongodb.net/task169?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


/*method to connect mongodb
mongoose.connect("<connectionStringfrommongodbatlas>", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

to create a notification if the connection to the db is successful or not*/
//connection came from the object declared form the packgaes and doesnt need us to declare
let db = mongoose.connection;
// We add this to show if the connection has error, it will show connection error in both terminal of browser and client
db.on('error', console.error.bind(console, 'Connection Error'));
//Successful connection to mongodb
db.once('open', () => console.log('Connected to MongoDB'));
//events keywords on once
const port = 4000;

app.use(express.json());
/*
group all task routes in /tasks
all endpoints in task routes files will start at /tasks
*/
app.use('/tasks', taskRoutes);
app.use('/users', userRoutes);

app.listen(port, () => console.log (`Server is now running at port ${port}`))











